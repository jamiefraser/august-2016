<?php
/**
 * Plugin Name: August Test 2016
 * Author: Jamie Fraser
 * Author URI: hello@jamiefraser.co.uk
 * Version: 3.0.0
 * Description: August Test Activities Plugin, completing the given assignments
 * Text Domain: _Omitted_
 **/
namespace AT2016;
$august_test_2016 = new Plugin();

class Plugin {

	static $folder_path;

	public function __construct()
	{
		self::set_folder( trailingslashit( dirname( __FILE__ ) ) );
		self::include_classes();

		Shortcode::init();

		add_action( 'init', array( __CLASS__, 'register_init_functions' ) );
		register_activation_hook( __FILE__ , array( __CLASS__ , 'activation_hooks' ) );
		add_action( 'admin_menu', array( __CLASS__, 'register_admin_pages' ) );
	}

	public static function set_folder( $folder )
	{
		self::$folder_path = $folder;
	}

	public static function include_classes()
	{
		self::include_class( 'shortcode.php' );
		self::include_class( 'posttype-songs.php' );
		self::include_class( 'taxonomy-genre.php' );
		self::include_class( 'table-formresponses.php' );
		self::include_class( 'menupage-responses.php' );
	}

	public static function is_acceptable_file_name( $class_file )
	{
		return preg_match( '/^[a-zA-Z][a-zA-Z0-9\-\_\.]{0,}\.php$/', $class_file ) == 1;
	}

	public static function include_class( $class_file )
	{
		//i dont want any ../ rubbish here
		if ( self::is_acceptable_file_name( $class_file ) ) {
			include self::$folder_path . 'classes/' . $class_file;
			return true;
		}

		return false;
	}

	public static function get_template_path ( $template_file )
	{
		//we're not including it here as we want to use some variables in these templates
		if ( self::is_acceptable_file_name( $template_file ) ) {
			return self::$folder_path . 'templates/' . $template_file;
		}

		return false;
	}

	public static function register_init_functions()
	{
		Songs::register();
		Genres::register();
	}

	public static function activation_hooks()
	{
		FormResponses::activation();
	}

	public static function register_admin_pages()
	{
		ResponsesMenuPage::register();
	}
}
