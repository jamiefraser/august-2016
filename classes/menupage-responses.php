<?php
namespace AT2016;

class ResponsesMenuPage {

	const MENU_PAGE_TITLE = 'Form Responses';
	const MENU_TITLE = 'Form Responses';
	//read? nothing else mentioned
	const MENU_CAPABILITY = 'read';
	const MENU_SLUG = 'at2016responses';
	//no menu icon at this time
	const MENU_ICON_URL = null;

	const DISPLAY_DATETIME_FORMAT = 'jS F Y @ H:i:s';

	public static function register() {
		$menu = add_menu_page(
			self::MENU_PAGE_TITLE,
			self::MENU_TITLE,
			self::MENU_CAPABILITY,
			self::MENU_SLUG,
			array( __CLASS__, 'menu_page_actual' ),
			self::MENU_ICON_URL
		);

		//$menu could then be used to enqueue scripts and styles
	}

	public static function menu_page_actual()
	{
		$page_title = self::MENU_PAGE_TITLE;
		$menu_slug = self::MENU_SLUG;
		$display_date_format = self::DISPLAY_DATETIME_FORMAT;

		$results_per_page = isset($_GET['resultsperpage']) ? $_GET['resultsperpage'] : 0;
		$page = isset($_GET['p']) ? $_GET['p'] : 1;

		//we dont want anything less than 0
		$results_per_page = max(0, intval($results_per_page));

		//we dont want anything less than 1
		$page = max(1, intval($page));

		$record_count = FormResponses::get_row_count();

		$this_page = array();
		$this_pagination = array();
		if ($record_count > 0) {
			$this_page = FormResponses::get_paged_responses($page, $results_per_page);
			$this_pagination = self::create_basic_pagination($page, $results_per_page, $record_count);
		}

		include Plugin::get_template_path( 'responses-menu-page.php' );

	}

	public static function create_basic_pagination($page, $results_per_page, $record_count)
	{
		$pagination_links = array();
		if ($results_per_page == 0) {
			$last_possible_page = 1;
		} else {
			$last_possible_page = ceil($record_count / $results_per_page);
		}
		if ($page == 1) {
			$pagination_links[] = array(
				'name' => 'First',
				'disabled' => true,
			);
			$pagination_links[] = array(
				'name' => 'Prev',
				'disabled' => true,
			);
		} else {
			$pagination_links[] = array(
				'name' => 'First',
				'link' => '?page=' . self::MENU_SLUG . '&resultsperpage=' . $results_per_page . '&p=1'
			);
			$pagination_links[] = array(
				'name' => 'Prev',
				'link' => '?page=' . self::MENU_SLUG . '&resultsperpage=' . $results_per_page . '&p=' . ($page-1)
			);
		}

		$pagination_links[] = array(
			'name' => $page,
			'disabled' => true,
		);

		if ($page >= $last_possible_page) {
			$pagination_links[] = array(
				'name' => 'Last',
				'disabled' => true,
			);
			$pagination_links[] = array(
				'name' => 'Next',
				'disabled' => true,
			);
		} else {
			$pagination_links[] = array(
				'name' => 'Next',
				'link' => '?page=' . self::MENU_SLUG . '&resultsperpage=' . $results_per_page . '&p=' . ($page+1)
			);
			$pagination_links[] = array(
				'name' => 'Last',
				'link' => '?page=' . self::MENU_SLUG . '&resultsperpage=' . $results_per_page . '&p=' . $last_possible_page
			);
		}
		return $pagination_links;
	}

}
