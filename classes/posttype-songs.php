<?php
namespace AT2016;

class Songs {

	const POST_TYPE_NAME = 'songs';
	const PUBLIC_POST_TYPE_NAME_SINGULAR = 'Song';
	const PUBLIC_POST_TYPE_NAME_PLURAL = 'Songs';

	public static function get_post_type_name()
	{
		//we could apply filters to the post type name, but its beyond scope for this?
		return self::POST_TYPE_NAME;
	}

	public static function get_singular_name()
	{
		return self::PUBLIC_POST_TYPE_NAME_SINGULAR;
	}

	public static function get_plural_name()
	{
		return self::PUBLIC_POST_TYPE_NAME_PLURAL;
	}

	public static function register()
	{
		register_post_type(
			self::get_post_type_name(),
			array(
				'capability_type' 		=> 'post',
				'description' 			=> 'Songs, lots and lots of songs',
				'has_archive' 			=> true,
				'hierarchical' 			=> false,
				'labels' 				=> self::get_post_type_labels(),
				'menu_position' 		=> null,
				'public' 				=> true,
				'publicly_queryable' 	=> true,
				'query_var' 			=> true,
				'rewrite' 				=> array(
					'slug' => self::get_post_type_name()
				),
				'show_in_menu' 			=> true,
				'show_ui' 				=> true,
				'supports' 				=> array(
					'title',
					'editor',
					'author',
					'thumbnail',
					'excerpt',
					'comments'
				)
			)
		);
	}

	public static function get_post_type_labels()
	{
		//I've purposely omitted the translatable functions for this

		$labels = array(
			'add_new' 				=> 'Add New',
			'add_new_item' 			=> 'Add New ' . self::get_singular_name(),
			'all_items' 			=> 'All ' . self::get_plural_name(),
			'edit_item' 			=> 'Edit ' . self::get_singular_name(),
			'menu_name' 			=> self::get_plural_name(),
			'name' 					=> self::get_plural_name(),
			'name_admin_bar' 		=> self::get_singular_name(),
			'new_item' 				=> 'New ' . self::get_singular_name(),
			'not_found' 			=> 'No ' . self::get_plural_name() . ' Found',
			'not_found_in_trash' 	=> 'No ' . self::get_plural_name() . ' Found in Trash',
			'parent_item_colon' 	=> 'Parent ' . self::get_plural_name(),
			'search_items' 			=> 'Search ' . self::get_plural_name(),
			'singular_name' 		=> self::get_singular_name(),
			'view_item' 			=> 'View ' . self::get_singular_name()
		);

		return apply_filters( 'AT2016-Songs-PTLabels', $labels );
	}
}
