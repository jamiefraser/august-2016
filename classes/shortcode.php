<?php
namespace AT2016;
use Exception;
class Shortcode {

	const CODE = 'authorform';
	const SHORTCODE_REGEX_CHECK = '/^[0-9a-zA-Z\-\_]{1,}$/';
	const AJAX_HOOK = 'AT2016FormSubmission';

	public static function init()
	{
		add_shortcode( self::get_shortcode_code(), self::get_handling_function() );
		add_action( self::get_ajax_hook(), array( __CLASS__, 'ajax' ) );
		add_action( self::get_ajax_nopriv_hook(), array( __CLASS__, 'ajax' ) );
		add_action( 'wp_enqueue_scripts', array( __CLASS__, 'ensure_jquery' ) );
	}

	public static function ensure_jquery()
	{
		wp_enqueue_script( 'jquery' );
	}

	public static function get_ajax_nopriv_hook()
	{
		return 'wp_ajax_nopriv_' . self::AJAX_HOOK;
	}

	public static function get_ajax_hook( $withPrefix = true )
	{
		return ($withPrefix ? 'wp_ajax_' : '') . self::AJAX_HOOK;
	}

	public static function get_shortcode_code()
	{
		//allow the option for the shortcode to be customised
		$shortcode = apply_filters( 'AT2016-Shortcode-Code', self::CODE );

		//we only want to allow sensible shortcodes, otherwise we're going to default to the original one
		if ( $shortcode == self::CODE || preg_match( self::SHORTCODE_REGEX_CHECK, $shortcode ) ) {
			return $shortcode;
		}

		return self::CODE;
	}

	public static function get_handling_function()
	{
		return array( __CLASS__, 'shortcode_handler' );
	}

	public static function shortcode_handler($args = array())
	{
		$defaultArguments = array(
			'namefield' => 1,
			'emailfield' => 1
		);

		$args = shortcode_atts( $defaultArguments, $args );

		//i dont like including big html blocks in classes if i can avoid it
		ob_start();
		$ajaxHook = self::get_ajax_hook(false);
		include Plugin::get_template_path( 'authorform.php' );
		return ob_get_clean();
	}

	public static function ajax()
	{
		$response = array(
			'success' => 0,
			'message' => ''
		);
		//other types of Exceptions are available
		try {
			if ( ! wp_verify_nonce( $_POST['_wpnonce'], 'authorformsubmish' ) ) {
				throw new Exception( 'Not Authorised' );
			}

			$email = isset( $_POST['email'] ) ? $_POST['email'] : '';
			$name = isset( $_POST['name'] ) ? $_POST['name'] : '';
			self::validate_submitted_name( $name );
			self::validate_submitted_email( $email );

			//thats it for the very basic email / name checks
			$submission = FormResponses::insert_entry( $name, $email );

			if ( ! $submission ) {
				throw new Exception( 'An error occured during submission, please try again later' );
			}

			$response['success'] = 1;
			$response['message'] = 'Thank you for your submission, we will be in touch soon?';

		} catch ( Exception $e ) {
			$response['success'] = 0;
			$response['message'] = $e->getMessage();
		}
		wp_send_json( $response );
	}

	private function validate_submitted_name($name)
	{
		//I read somewhere that doing strlen is computationally slightly harder than doing [0] for ensuring something is entered
		if ( ! isset( $name[0] ) ) {
			throw new Exception( 'Please enter a name' );
		}

		if ( isset( $name[64] ) ) {
			throw new Exception( 'The name entered is too long, perhaps something shorter?' );
		}
	}

	private function validate_submitted_email($email)
	{
		//this is only basic email filtering, you could check if the domain has an mx record
		if ( ! filter_var( $email, FILTER_VALIDATE_EMAIL ) ) {
			throw new Exception( 'Invalid Email' );
		}

		if ( FormResponses::has_email_been_used( $email ) ) {
			throw new Exception( 'That email has already been used' );
		}
	}


}
