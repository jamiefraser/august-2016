<?php
namespace AT2016;

class FormResponses {

	const TABLE_NAME = 'formresponses';
	const INSTALLED_VERSION_OPTION_KEY = 'AT2016-table-version';
	const TABLE_VERSION = 2;

	public static function get_full_table_name()
	{
		global $wpdb;
		$table_name = apply_filters( 'AT2016-tablename', self::TABLE_NAME );
		return $wpdb->prefix . $table_name;
	}

	private static function get_installed_database_version()
	{
		$option_version = get_option( self::INSTALLED_VERSION_OPTION_KEY, 0 );

		if ( $option_version === 0 ) {
			//im not autoloading it, because we dont really need to be loading this unless it is an activation
			add_option( self::INSTALLED_VERSION_OPTION_KEY, 0, null, false );
		}

		return $option_version;
	}

	public static function set_installed_database_version()
	{
		return update_option( self::INSTALLED_VERSION_OPTION_KEY, self::TABLE_VERSION );
	}

	public static function activation()
	{
		if ( self::get_installed_database_version() != self::TABLE_VERSION ) {
			$sql = 'CREATE TABLE ' . self::get_full_table_name() . ' (
				aid INT UNSIGNED NOT NULL AUTO_INCREMENT,
				name VARCHAR(64) NOT NULL,
				email VARCHAR(256) NOT NULL,
				datestamp DATETIME NOT NULL DEFAULT 0,
				PRIMARY KEY (aid),
				UNIQUE KEY (email)
			)';

			//READY THE UPGRADE
			require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
			dbDelta( $sql );

			$updated = self::set_installed_database_version();
			//at this point we could display some sort of notification if updated is false, but lets assume we dont need this at this point
		}
	}

	public static function insert_entry($name, $email)
	{
		global $wpdb;
		return $wpdb->insert(
			self::get_full_table_name(),
			array(
				'name' => $name,
				'email' => $email,
				'datestamp' => date('Y-m-d H:i:s')
			),
			array(
				'%s',
				'%s',
				'%s'
			)
		);
	}

	public static function has_email_been_used($email)
	{
		global $wpdb;
		$sql = $wpdb->prepare( 'SELECT 1 AS present FROM ' . self::get_full_table_name() . ' WHERE email = %s', $email );
		return intval( $wpdb->get_var( $sql ) ) > 0;
	}

	public static function get_row_count()
	{
		global $wpdb;
		return intval( $wpdb->get_var( 'SELECT COUNT(1) FROM ' . self::get_full_table_name() ) );
	}

	public static function get_paged_responses($page = 1, $per_page = 0)
	{
		global $wpdb;
		//also select * :)
		$sql = 'SELECT aid, name, email, datestamp FROM ' . self::get_full_table_name();

		//don't want negative values for these:
		$per_page = max(0, intval($per_page));
		$page = max(1, intval($page));


		if ($per_page > 0) {
			$page --; //well, its for offset
			$offset = $per_page * $page;
			$sql .= sprintf(' LIMIT %d, %d', $offset, $per_page);
		}

		$records = $wpdb->get_results($sql, OBJECT);
		return $records;
	}
}
