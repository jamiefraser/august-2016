<?php
namespace AT2016;
use Exception;
class Genres {

	const TAXONOMY_NAME = 'genres';
	const PUBLIC_TAXONOMY_NAME_SINGULAR = 'Genre';
	const PUBLIC_TAXONOMY_NAME_PLURAL = 'Genres';

	public static function get_taxonomy_name()
	{
		//we could apply filters to the post type name, but its beyond scope for this?
		return self::TAXONOMY_NAME;
	}

	public static function get_singular_name()
	{
		return self::PUBLIC_TAXONOMY_NAME_SINGULAR;
	}

	public static function get_plural_name()
	{
		return self::PUBLIC_TAXONOMY_NAME_PLURAL;
	}

	public static function register()
	{
		register_taxonomy(
			self::get_taxonomy_name(),
			array(
				Songs::get_post_type_name()
			),
			array (
				'hierarchical'      => true,
				'labels'            => self::get_taxonomy_labels(),
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array(
					'slug' => self::get_taxonomy_name()
				),
			)
		);

		//so it states these ones should be there by default, however it doesnt yet state if they should be removable, right?
		//at any rate, for now I will do it so that you can't remove them, just accept it!
		//okay, at least until I've asked about it, but I'm going to continue this rather than wait for a response, so it may change in later tags
		self::register_initial_terms();
	}

	public static function get_taxonomy_labels()
	{
		//I've purposely omitted the translatable functions for this for now

		$labels = array(
			'add_new_item'      => 'Add New ' . self::get_singular_name(),
			'all_items'         => 'All ' . self::get_plural_name(),
			'edit_item'         => 'Edit ' . self::get_singular_name(),
			'menu_name'			=> self::get_plural_name(),
			'name'              => self::get_plural_name(),
			'new_item_name'     => 'New ' . self::get_singular_name() . ' Name',
			'not_found' 		=> 'No ' . self::get_plural_name() . ' Found',
			'parent_item'       => 'Parent ' . self::get_singular_name(),
			'parent_item_colon' => 'Parent ' . self::get_singular_name() . ':',
			'search_items'      => 'Search ' . self::get_plural_name(),
			'singular_name'     => self::get_singular_name(),
			'update_item'       => 'Update ' . self::get_singular_name(),
		);

		return apply_filters( 'AT2016-Genres-TaxLabels', $labels );
	}

	public static function register_initial_terms()
	{
		try {

			$musical_theatre_term = term_exists( 'Musical Theatre', self::get_taxonomy_name() );
			if ( $musical_theatre_term === 0 || $musical_theatre_term === null ) {
				$musical_theatre_term = wp_insert_term(
					'Musical Theatre',
					self::get_taxonomy_name(),
					array (
						'description' => 'A Description about Musical Theatre',
					)
				);

				if ( is_wp_error( $musical_theatre_term ) ) {
					throw new Exception('Failed to register Musical Theatre');
				}
			}

			$sound_of_music_term = term_exists(
				'Sound Of Music',
				self::get_taxonomy_name(),
				$musical_theatre_term['term_id']
			);

			if ( $sound_of_music_term === 0 || $sound_of_music_term === null ) {
				$sound_of_music_term = wp_insert_term(
					'Sound of Music',
					self::get_taxonomy_name(),
					array (
						'description' => 'The plugin is alive with the sound of music',
						'parent' => $musical_theatre_term['term_id']
					)
				);

				if ( is_wp_error( $musical_theatre_term ) ) {
					throw new Exception( 'Failed to register Sound of Music' );
				}
			}

			return true;

		} catch ( Exception $e ) {
			//do nothing for now
			return false;
		}
	}
}
