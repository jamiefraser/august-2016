=== August Test 2016 ===

Whilst this doesn't comply with proper WordPress readme standards, I'm going to use it for notes about my submission

== General ==
* For speed and because it was omitted from this brief, I have decided to leave off translation / l18n support, allowing for enhanced readability
* I have also included items which may slow down execution, but the difference is negligble at this stage, for example using a function to get a name, rather than just hard coding the name
* I have not included all uses of actions / filters one may find useful in a plugin, mostly because a) it wasn't scoped, b) if this was a bespoke plugin they may not be needed, c) in the case of these reviews, they would be obsolete
* I have tried to maintain long array syntax rather than the shortened [];

== Part One ==

= i =
* The code is located within classes/shortcode.php, with the main template for the form being brought in from templates/authorform.php for enhanced readability
* On page load it registers the shortcode with the callback function

= ii =
* The code is located with classes/posttype-songs.php
* It was noted in the brief that "WordPress Admin users can manage", this was taken as anyone with access to the WordPress admin area (so following the general add_post / edit_post capabilities) at the time of writing, however I will question this and factor changes in at the end should they be needed

= iii =
* The code is located within classes/taxonomy-genre.php
* It was noted in the brief that "WordPress Admin users can manage", this was taken as above, but applying the relevant taxonomy/term editing roles, again these will be questioned and changes factored in
* For speed, I made it replace the default terms if they are ever not present, I would be inclined to have stored it as an option also.


== Part Two ==

* After tagging, I noticed I had missed out a Use for the Exception class within the taxonomy-genre.php from part 3 above, I have added it in, this was missed due to it being in a section that only fails during exceptional circumstances

= i =
* The code is located with table-formresponses.php, nothing overly fancy

= ii =
* Code modifications with made to classes/shortcode.php and it's form template in templates/authorform.php
* I decided to include the css and js in line in the template for simplicity

== Part Three ==

= i =
* The update to the able is located within table-formresponses.php using dbdelta to upgrade
* Also added datestamp column into form submissions within the insert_entries method

= ii =
* The bulk of the code is located in classes/menupage-responses.php, using templates/responses-menu-page to output the html
* Basic pagination has been added to the result count
