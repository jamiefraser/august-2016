<style>
form #author_form_message {
	color:white;
	font-weight:bold;
	margin-bottom:10px;
	padding:10px;
	text-align:center;
}
form div.message_success {
	background:#090;
}
form div.message_error {
	background:#900;
}
</style>
<form id="authorform" action='#' method='post'>
	<?php wp_nonce_field( 'authorformsubmish' ); ?>
	<input type='hidden' name='action' value='<?php echo $ajaxHook; ?>' />
	<div style='display:none' class='' id='author_form_message'></div>


	<p><strong>Fill Out This Form!</strong></p>

	<p><label for='name'>Your Name  <sup style='color:red'>*</sup></label><br />
		<input type='text' name='name' id='name' maxlength='128' required='required' /></p>

	<p><label for='email'>Your Email <sup style='color:red'>*</sup></label><br />
		<input type='email' name='email' id='email' required='required' /></p>

	<p><input type='submit' value='Submit!' /> <sup><span style='color:red'>*</span> = required</sup></p>

</form>

<script>
	jQuery(document).ready(function($){
		var formPostUrl = '<?php echo admin_url( 'admin-ajax.php' ); ?>';
		var form = $("form#authorform");
		var message = $("div#author_form_message");

		form.bind('submit', function(e){
			//no more going to #
			e.preventDefault();
			e.stopPropagation();

			formData = form.serialize();
			message.hide();
			message.removeClass('message_success').removeClass('message_error');
			$.post(formPostUrl, formData).done(function(r) {
				if (r.success) {
					message.addClass('message_success');
				} else {
					message.addClass('message_error');
				}
				message.text(r.message).show();
			});
		});

	});

</script>
