<div class='wrap'>
	<h1><?php echo $page_title; ?></h1>

	<form action='' method='get'>
		<input type='hidden' name='page' value='<?php echo $menu_slug; ?>' />
		<p>
			<label for='resultsperpage'>Results Per Page</label>
			<select name='resultsperpage' id='resultsperpage'>
				<option value='0'<?php selected('0', $results_per_page);?>>All</option>
				<option value='10'<?php selected('10', $results_per_page);?>>10</option>
				<option value='20'<?php selected('20', $results_per_page);?>>20</option>
				<option value='50'<?php selected('50', $results_per_page);?>>50</option>
				<option value='100'<?php selected('100', $results_per_page);?>>100</option>
			</select>
			<input type='submit' class='button button-primary' value='Update' />
		</p>
	</form>
	<p><strong>There are [<?php echo $record_count;?>] entries in the table</strong></p>

	<table class='wp-list-table widefat fixed striped'>
		<thead>
			<tr>
				<th>Entry #</th>
				<th>Name</th>
				<th>Email</th>
				<th>Datestamp</th>
			</tr>
		</thead>
		<tbody>
			<?php if ( count( $this_page ) > 0 ) {
				foreach ( $this_page as $record ) {
					$datestamp = new \DateTime($record->datestamp);
					printf(
						'<tr><td>%.0d</td><td>%s</td><td>%s</td><td>%s</td></tr>',
						$record->aid,
						htmlspecialchars($record->name),
						htmlspecialchars($record->email),
						$datestamp->format($display_date_format)
					);
				}
			} else {
				echo '<tr class="no-items"><td colspan="4">No Results</td></tr>';
			}?>
		</tbody>
		<tfoot>
			<tr>
				<th>Entry #</th>
				<th>Name</th>
				<th>Email</th>
				<th>Datestamp</th>
			</tr>
		</tfoot>
	</table>
	<?php if ( count( $this_pagination ) > 0) { ?>
		<div class="tablenav">
			<div class='tablenav-pages'>
				<span class="displaying-num"><?php echo count($this_page)?> items</span>
				<span class="pagination-links">
			<?php foreach( $this_pagination as $item ) {
				if ($item['disabled'] == 1) {
					printf('<span class="tablenav-pages-navspan">%s</span>', $item['name']);
					continue;
				}

				printf('<a class="next-page" href="%s">%s</a>', $item['link'], $item['name']);
			} ?>
				</span>
			</div>
		</div>
	<?php } ?>
</div>
